#!/bin/bash

set -e

if [ -n $TEZOS_BASE_DIR ]
then
  BASE_DIR="--base-dir $TEZOS_BASE_DIR"
fi

AMOUNT=$1
ACCOUNT1=$2
ACCOUNT2=$3
START=$4
END=$5


if [ -z "$START" ] || [ -z "$END" ]
then
  echo "Transfer $AMOUNT XTZ from $ACCOUNT1 to $ACCOUNT2..."
  tezos-client $BASE_DIR tranfer -w 1 $AMOUNT from $ACCOUNT1 to $ACCOUNT2 --burn-cap 0.257
  echo "Done."

else
  echo "Transfer $AMOUNT XTZ from $ACCOUNT1 to $ACCOUNT2[$START..$END]..."
  CMD=""
  for INDEX in $(seq $START $END);
    do
    if [ $INDEX -ne $END ]
    then
      CMD+="tezos-client $BASE_DIR -w 1 transfer $AMOUNT from $ACCOUNT1 to $ACCOUNT2$INDEX --burn-cap 0.257 & "
    else
      CMD+="tezos-client $BASE_DIR -w 1 transfer $AMOUNT from $ACCOUNT1 to $ACCOUNT2$INDEX --burn-cap 0.257 & wait"
    fi
  done

  echo $CMD
  eval $CMD
  echo "Done."
fi


exit
