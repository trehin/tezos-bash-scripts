#!/bin/bash

set -e

if [ -n $TEZOS_BASE_DIR ]
then
  BASE_DIR="--base-dir $TEZOS_BASE_DIR"
fi

ACTION=$1


if [ "$ACTION" = "create" ]
then

  NAME=$2
  START=$3
  END=$4

  if [ -z "$START" ] || [ -z "$END" ]
  then
    echo "Create '$NAME' account..."
    tezos-client $BASE_DIR gen keys $NAME
    echo "Done."

  else
    echo "Create '$NAME[$START..$END]' accounts..."
    for INDEX in $(seq $START $END);
      do
      echo $INDEX
      tezos-client $BASE_DIR gen keys $NAME$INDEX
    done
  fi


elif [ "$ACTION" = "delete" ]
then

  NAME=$2
  START=$3
  END=$4

  if [ -z "$START" ] || [ -z "$END" ]
  then
    echo "Delete '$NAME' account..."
    tezos-client $BASE_DIR forget address $NAME --force
    echo "Done."

  else
    echo "Delete '$NAME[$START..$END]' accounts..."

   for INDEX in $(seq $START $END);
     do
     echo $INDEX
     tezos-client $BASE_DIR forget address $NAME$INDEX --force
   done
  fi


elif [ "$ACTION" = "activate" ]
then

  NAME=$2
  START=$3
  END=$4

  if [ -z "$START" ] || [ -z "$END" ]
  then
    echo "Delete '$NAME' account..."
    tezos-client $BASE_DIR forget address $NAME --force
    echo "Done."

  else
    echo "Delete '$NAME[$START..$END]' accounts..."

   for INDEX in $(seq $START $END);
     do
     echo $INDEX
     tezos-client $BASE_DIR forget address $NAME$INDEX --force
   done
  fi

else
  printf "Usage: create_account.sh <COMMAND> <NAME> [<NAME_START_INDEX> <NAME_END_INDEX>]\n\n"
  printf "Example:\n  create_account.sh create my_wallet 1 10\n\n"
  printf "Commands:\n"
  printf "  create	Create new account\n"
  printf "  delete	Delete new account\n"
fi

exit
