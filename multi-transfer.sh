#!/bin/bash

set -e

if [ -n $TEZOS_BASE_DIR ]
then
  BASE_DIR="--base-dir $TEZOS_BASE_DIR"
fi

AMOUNT=$(($1*1000))
ACCOUNT1=$2
ACCOUNT2=$3
START=$4
END=$5

FEE="1420"
STYLE_START="\e[1m"
STYLE_END="\e[0m"

BRANCH_HASH=`tezos-client rpc get http://127.0.0.1:8732/chains/main/blocks/head/hash`
BRANCH_HASH="${BRANCH_HASH%\"}"
BRANCH_HASH="${BRANCH_HASH#\"}"
echo -e "BRANCH HASH: $STYLE_START$BRANCH_HASH"

ACCOUNT1_HASH=`jq '.[] | select(.name=='\"$ACCOUNT1\"') | .value' $TEZOS_BASE_DIR/public_key_hashs`
ACCOUNT1_HASH="${ACCOUNT1_HASH%\"}"
ACCOUNT1_HASH="${ACCOUNT1_HASH#\"}"
echo -e "${STYLE_END}ACCOUNT1 HASH: $STYLE_START$ACCOUNT1_HASH ${STYLE_END}($ACCOUNT1)"

ACCOUNT2_HASH=`jq '.[] | select(.name=='\"$ACCOUNT2\"') | .value' $TEZOS_BASE_DIR/public_key_hashs`
ACCOUNT2_HASH="${ACCOUNT2_HASH%\"}"
ACCOUNT2_HASH="${ACCOUNT2_HASH#\"}"
echo -e "${STYLE_END}ACCOUNT2 HASH: $STYLE_START$ACCOUNT2_HASH ${STYLE_END}($ACCOUNT2)"

COUNTER=`tezos-client rpc get http://127.0.0.1:8732/chains/main/blocks/head/context/contracts/$ACCOUNT1_HASH/counter`
COUNTER="${COUNTER%\"}"
COUNTER="${COUNTER#\"}"
echo -e "${STYLE_END}NEXT COUNTER: $STYLE_START$((COUNTER+1))$STYLE_END (current: $COUNTER)"

PROTO_HASH=`tezos-client rpc get /chains/main/blocks/head/header | jq '.protocol'`
PROTO_HASH="${PROTO_HASH%\"}"
PROTO_HASH="${PROTO_HASH#\"}"
echo -e "${STYLE_END}PROTO HASH: $STYLE_START$PROTO_HASH"


OP_BYTES=`curl -sb POST \
  http://127.0.0.1:8732/chains/main/blocks/head/helpers/forge/operations \
  -H 'Cache-Control:no-cache' \
  -H 'Content-Type:application/json' \
  -H 'charset=utf-8' \
  -d '{
    "contents": [
      {
        "kind": "transaction",
        "amount": '\"$AMOUNT\"',
        "source": '\"$ACCOUNT1_HASH\"',
        "destination": '\"$ACCOUNT2_HASH\"',
        "storage_limit": "0",
        "gas_limit": "10000",
        "fee": '\"$FEE\"',
        "counter": '\"$((COUNTER+1))\"'
      }
    ],
    "branch": '\"$BRANCH_HASH\"'
  }'`
echo TTTTTTTTT $OP_BYTES
OP_BYTES="${OP_BYTES%\"}"
OP_BYTES="${OP_BYTES#\"}"
echo -e "${STYLE_END}OPERATION BYTES: $STYLE_START$OP_BYTES"

SIGNED_OP=`tezos-client $BASE_DIR sign bytes 0x03$OP_BYTES for $ACCOUNT1`
SIGNED_OP="${SIGNED_OP#Signature: }"
echo -e "${STYLE_END}SIGNED OPERATION: $STYLE_START$SIGNED_OP"

PREAPPLY_OP=`curl -sb POST \
  http://127.0.0.1:8732/chains/main/blocks/head/helpers/preapply/operations \
  -H 'Cache-Control:no-cache' \
  -H 'Content-Type:application/json' \
  -d '[{
    "protocol": '\"$PROTO_HASH\"',
    "branch": '\"$BRANCH_HASH\"',
    "contents": [
      {
        "kind": "transaction",
        "amount": '\"$AMOUNT\"',
        "source": '\"$ACCOUNT1_HASH\"',
        "destination": '\"$ACCOUNT2_HASH\"',
        "storage_limit": "0",
        "gas_limit": "10000",
        "fee": '\"$FEE\"',
        "counter": '\"$((COUNTER+1))\"'
      }
    ],
    "signature": '\"$SIGNED_OP\"'
  }]'`
echo -e "${STYLE_END}PREAPPLY OPERATION:"
echo "$PREAPPLY_OP" | jq

decodeBase58() {
  declare -a base58=(
      1 2 3 4 5 6 7 8 9
    A B C D E F G H   J K L M N   P Q R S T U V W X Y Z
    a b c d e f g h i j k   m n o p q r s t u v w x y z
  )
  unset dcr; for i in {0..57}; do dcr+="${i}s${base58[i]}"; done
  declare ec_dc='
I16i7sb0sa[[_1*lm1-*lm%q]Std0>tlm%Lts#]s%[Smddl%x-lm/rl%xLms#]s~
483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8
79BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798
2 100^d14551231950B75FC4402DA1732FC9BEBF-so1000003D1-ddspsm*+sGi
[_1*l%x]s_[+l%x]s+[*l%x]s*[-l%x]s-[l%xsclmsd1su0sv0sr1st[q]SQ[lc
0=Qldlcl~xlcsdscsqlrlqlu*-ltlqlv*-lulvstsrsvsulXx]dSXxLXs#LQs#lr
l%x]sI[lpSm[+q]S0d0=0lpl~xsydsxd*3*lal+x2ly*lIx*l%xdsld*2lx*l-xd
lxrl-xlll*xlyl-xrlp*+Lms#L0s#]sD[lpSm[+q]S0[2;AlDxq]Sdd0=0rd0=0d
2:Alp~1:A0:Ad2:Blp~1:B0:B2;A2;B=d[0q]Sx2;A0;B1;Bl_xrlm*+=x0;A0;B
l-xlIxdsi1;A1;Bl-xl*xdsld*0;Al-x0;Bl-xd0;Arl-xlll*x1;Al-xrlp*+L0
s#Lds#Lxs#Lms#]sA[rs.0r[rl.lAxr]SP[q]sQ[d0!<Qd2%1=P2/l.lDxs.lLx]
dSLxs#LPs#LQs#]sM[lpd1+4/r|]sR
';
  echo -n "$1" | sed -e's/^\(1*\).*/\1/' -e's/1/00/g' | tr -d '\n'
  dc -e "$dcr 16o0$(sed 's/./ 58*l&+/g' <<<$1)p" |
  while read n; 
  do 
    echo -n ${n/\\/};
  done
}

DECODED_SIGNED_OP=$(decodeBase58 $SIGNED_OP)
	echo $DECODED_SIGNED_OP > ./test1
DECODED_SIGNED_OP="${DECODED_SIGNED_OP#9F5CD8612}"
echo $DECODED_SIGNED_OP >> ./test1
DECODED_SIGNED_OP="${DECODED_SIGNED_OP%????????}"
echo $DECODED_SIGNED_OP >> ./test1
echo -e "${STYLE_END}HEXADECIMAL SIGNED OPERATION: ${STYLE_START}$DECODED_SIGNED_OP"

SIGNED_OP_BYTES=$OP_BYTES${DECODED_SIGNED_OP,,}
echo $SIGNED_OP_BYTES > ./test1
echo -e "${STYLE_END}SIGNED OPERATION BYTES: ${STYLE_START}$SIGNED_OP_BYTES"

INJECT_OP=`curl -sb POST \
  http://127.0.0.1:8732/injection/operation \
  -H 'Cache-Control:no-cache' \
  -H 'Content-Type:application/json' \
  -H 'charset=utf-8' \
  -d '"\"$SIGNED_OP_BYTES\""'`
echo -e "${STYLE_END}INJECTED OPERATION HASH: ${STYLE_START}$INJECT_OP"


exit
