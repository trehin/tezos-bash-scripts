#!/bin/bash

set -e 

if [ -n $TEZOS_BASE_DIR ]
then
  BASE_DIR="--base-dir $TEZOS_BASE_DIR"
fi

AMOUNT=$1
ACCOUNT=$2
START=$3
END=$4

CMD=""

while true
do
    for INDEX in $(seq $START $END);
    do
        if [ $INDEX -ne $END ]
        then
            CMD+="tezos-client $BASE_DIR -w 1 transfer $AMOUNT from $ACCOUNT$INDEX to $ACCOUNT$(($INDEX+1)) --burn-cap 0.257 & "
        else
            CMD+="tezos-client $BASE_DIR -w 1 transfer $AMOUNT from $ACCOUNT$INDEX to $ACCOUNT$START --burn-cap 0.257 & wait"
        fi
    done

    echo $CMD
    eval $CMD
    CMD=""
    echo "================= END ====================="
done

exit
